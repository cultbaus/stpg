+++
title = "Setting your new tab page on Firefox"
description = "Learn how to set your new tab to open your local startpage on Firefox without the use of an extension!"
+++

# Setting your new tab page on Firefox

{% banner() %}
Most of the information here is now outdated, a new guide is coming...
{% end %}

{% banner() %}
Not using Firefox? Check out our guide for[Chrome](/guides/chrome-startpage)
{% end %}

In general, setting your new tab page to a custom HTML file has become a bit
bothersome. There _are_ extensions out there for both Firefox and Chrome that
fulfil some requirements and I do still recommend that people who are
not comfortable editing files directly stick to those.

[New tab override](https://addons.mozilla.org/en-GB/firefox/addon/new-tab-override/)
is the best one you can find for Firefox but the core limitation is that it can
only access _a single file_ so you cannot use startpages that rely on other
local files (scripts, stylesheets).

## A bit of history[#](#a-bit-of-history)

I won't dwell here long, promise! You can
[skip to the actual set up](#autoconfigjs).

A number of years ago [an issue](https://bugzilla.mozilla.org/show_bug.cgi?id=1118285)
regarding the then-popular method of setting a new tab page was raised.
`browser.newtab.url` was exposed to the system in order to make it easier for
extension users to set new tabs up.

The issue here being installers and hijackers (running simple scripts) could
have overwritten your new tab page into something they wanted you to see and
steal your data.

After that it became increasingly more difficult to set up new tab pages, as
Mozilla was quite keen on pushing its own tiled startpage and the appeal of a
"fully custom" startpage never saw much popularity.

Alas, there are still plenty of ways of doing it. I'll present you with a
flexible and simple way of setting any file as a new tab page.

## autoconfig.js[#](#autoconfigjs)

### The easy linux way

If you're on Linux (currently tested on Debian and Void) you can easily make
this work using our
[handy script](https://gitlab.com/vallode/firefox-new-tab-script).
Just run the script:

```
bash -c "$(wget -qO - 'https://stpg.tk/firefox-new-tab-script')"
```

### The normal way

As far as I understand, Mozilla wanted to expose most of the preferences within
the browser to sysadmins who might want hard-code certain conditions into the
browser.

This helps us a lot, and is pretty simple to set up...

You need to navigate to your Firefox install in your preferred way
(file manager, command line, etc.).

* Windows: `C:\Program Files\Mozilla Firefox\`
* macOS: `/Applications/Firefox.app/Contents/MacOS`
* Linux: `/opt/firefox/`

Note: the Linux location is just where _my copy_ was installed. As always with
*nix systems, your mileage _will_ vary.

Note for macOS: a user has reported that their `defaults` folder was found in
`/Applications/Firefox.app/Contents/Resources/`, use this path if your defaults
don't exist elsewhere.

Once you've located your installation folder, navigate over to `defaults/pref`
(this could also be `defaults/preferences`). Here you need to create a file
named `autoconfig.js` with the following content:

```
pref("general.config.filename", "mozilla.cfg");
pref("general.config.obscure_value", 0);
```

With that done, navigate _out_ of the folder and back to the installation
directory (the same folder that has the `defaults` folder inside it).

Here you need to create a file named `mozilla.cfg` and populate it with the
following code:

```
// Any comment. You must start the file with a single-line comment!

let { classes:Cc, interfaces:Ci, utils:Cu } = Components;

try {
  Cu.import("resource:///modules/AboutNewTab.jsm");
  let newTabURL = "PATH_TO_STARTPAGE_REPLACE_ME";
  AboutNewTab.newTabURL = newTabURL;
} catch(e) { Cu.reportError(e); }
```

`PATH_TO_STARTPAGE_REPLACE_ME` is so aptly named because you should replace it
with the file path to your startpage. The _simplest_ way I know of getting the
full path is simply dragging your `index.html` (from your startpage) into your
browser and copying the path.

If you have whitespaces in your path ``, you will need to replace them with
`%20` in order for Firefox to accept it.

For example, my startpage has the path:
`file:///home/user/Documents/startpage/index.html`

Restart your browser, open a new tab, and bask in your achievement!

#### A note on autofocus

Firefox steals your focus to the address bar when you enter a new tab, there is
a way to fix this reliably with our method. All we have to do is add these lines
to your existing `mozilla.cfg` file:

```
// Auto focus new tab content
try {
  Cu.import("resource://gre/modules/Services.jsm");
  Cu.import("resource:///modules/BrowserWindowTracker.jsm");

  Services.obs.addObserver((event) => {
    window = BrowserWindowTracker.getTopWindow();
    window.gBrowser.selectedBrowser.focus();
  }, "browser-open-newtab-start");
} catch(e) { Cu.reportError(e); }
```

There may be caveats with this method, but it works nicely in combination to the
`autofocus` attribute on your `<input>` elements.

Don't have a startpage? Check out [our guides](/guides) on some simple
startpages you can make within a day :)
