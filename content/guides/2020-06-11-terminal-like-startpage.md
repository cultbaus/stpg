+++
title = "Terminal-like startpage guide"
image = "assets/images/terminal-like-startpage.png"
description = "Together we create a very simple but satisfying startpage that emulates the style of a *nix terminal!"
+++

# Terminal-like startpage

We're going to create a really nice and simple terminal-like startpage. Keep in
mind in order to keep this sleek we will **not** be including any
functionality beyond visual aesthetics.

That being said, if you are coming here with very little knowledge of CSS/HTML I
think it's a good idea to read through the _entirety_ of MDN's
[Getting started with the web](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web).
If you are new to web development, this will significantly increase your
understanding of _everything_ going on.

If you do not want to increase your knowledge that much, the bare bones on
[HTML](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics)
and [CSS](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/CSS_basics) will do well...

{% banner() %}
This guide was created on a livestream in our [discord server](https://discord.gg/qpFVrzg)!
{% end %}

## mkdir startpage[#](#mkdir-startpage)

![A terminal-like startpage](/images/terminal-like-startpage.png)
_- The startpage we are making_

Create a single file on your computer and name it `index.html`. The name of the
file actually doesn't matter too much, what really matters is the ending
`.html`, always include that.

Open this file in a text editor of your choice (We recommend using
[VS Code](https://code.visualstudio.com/)). You should be faced with... an empty
file.

Now time to start our development! Type out (better) or copy (worse) the HTML
below:

```html-new
<!DOCTYPE html>
<html>
<head>
  <title>user@home &gt; newtab</title>
</head>
<body>
  <p><span>user@home &gt; </span>newtab</p>
</body>
</html>
```

_We need to use `&gt;` as a replacement for the `>` symbol because it might
confuse certain browsers!_

If you look at the text above and don't understand it, please read the
[first paragraph again](#terminal-like-startpage).

Now drag this file over to your browser, you should be able to see a page
with just the test "user@home > newtab" displayed in the top left corner.

Not much of a **startpage**, we need a few links to our favourite websites.

```html
...
<body>
  <p><span>user@home &gt; </span>newtab</p>

```

```html-new
  <a href="">stpg.tk</a>
  <a href="">news</a>
  <a href="">ff send</a>
```

```html
</body>
...
```

_Note I use ellipsis `...` to remove text that is not relevant to what we are
changing. The full HTML is at the bottom of the page._

With all that said and done, you should have something that looks like this:

![A very basic startpage](/images/terminal-like-startpage-1.png)
_- Plain-text is great!_

## Terminals 101[#](#terminals-101)

We're going to make this look a lot more like a terminal right now. Let's get
to some styling.

With your file opened, we are going to be adding these CSS rules:

```html
...
<head>
  <title>user@home > newtab</title>

```

```html-new
  <style>
    html {
      align-items: center;
      display: flex;
      height: 100%;
      justify-content: center;
    }
  </style>
```

```html
</head>
...
```

Refresh the page where you opened your file or simply drag it over again if you
closed it, you should now see all of your text is centered in the middle of the
page. Well done for making it this far.

We're going to make a change to our HTML structure, this will help us style
individual elements quicker.

```html
...
<body>
  <p><span>user@home &gt; </span>startpage</p>

```

```html-new
  <nav>
    <ul>
      <li>general</li>
      <li><a href="">stpg.tk</a></li>
      <li><a href="">news</a></li>
      <li><a href="">ff send</a></li>
    </ul>
  </nav>

  <p><span>user@home &gt; </span>_</p>
```

```html
</body>
...
```

Notice we added the extra indicator at the bottom, makes it look like more of a
terminal!

Now let's make it a bit more coherent by adding some general padding and colors,
add these rules in the `<head>` block of our document:

```html
...
  <style>
    html {
      align-items: center;
```

```html-new
      background-color: #f7eee4;
      color: #c5c8c6;
```

```html
      display: flex;
      height: 100%;
      justify-content: center;
    }

```

```html-new
    body {
      background-color: #1d1f21;
      border-radius: 2px;
      color: #c5c8c6;
      display: flex;
      flex-direction: column;
      min-height: 30ch;
      min-width: 50vw;
      padding: 2em;
    }

    p {
      margin: 0;
    }

    p span {
      color: #b5bd68;
    }

    a {
      color: inherit;
      text-decoration: none;
    }
```

```html
  </style>
</head>
...
```

Honestly, this looks pretty dang good already. Let's make the link groups look
a little nicer.

```html
...
    a {
      color: inherit;
      text-decoration: none;
    }

```

```html-new
    a:focus,
    a:hover {
      font-weight: bold;
    }

    ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }

    nav {
      column-gap: 2em;
      display: grid;
      grid-template-columns: auto auto auto auto;
      padding: 1em 0;
    }
```

```html
  </style>
</head>
```

![A very basic startpage](/images/terminal-like-startpage-2.png)
_- Looking very cosy!_

Right, we added quite a bit so you might like to take a quick look over
everything and make sure you understand it all. If any rules look weird, it's
simple to search `<rule name> MDN`, i.e `column-gap MDN`.

If `display: flex` and `display: grid` confuses you, worry not! It confuses a
lot of people.

Have a look at MDN's
[Guide to grid layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)
, as well as
[Learn flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox).

## A single grain of rice[#](#a-single-grain-of-rice)

We have quite the startpage here, let's add a few last touches to make a house
a home.

```html
...
  <style>
    html {
      align-items: center;
      background-color: #f7eee4;
      color: #c5c8c6;
      display: flex;
```

```html-new
      font: 22px/1 "Inconsolata", monospace;
```

```html
height: 100%; justify-content: center; } ...
```

The font `Inconsolata` should be on your system, if your page does not look
similar to the screenshots you might have to install it. You can always look
for another font, too! Check out MDN's
[guide on font families](https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_text/Fundamentals#Font_families)

Let's add a subtle shadow to the terminal:

```html
...
  <style>
    body {
      background-color: #1d1f21;
      border-radius: 2px;
```

```html-new
      box-shadow: 1px 1px 6px #1d1f2187;
```

```html
      color: #c5c8c6;
      display: flex;
      flex-direction: column;
      min-height: 30ch;
      min-width: 50vw;
      padding: 1em;
    }
...
```

We are going to give some color to the link categories!

```html
...
  <style>
    nav {
      column-gap: 2em;
      display: grid;
      grid-template-columns: auto auto auto auto;
      padding: 1em 0;
    }

```

```html-new
    ul:nth-child(5n + 1) li:first-child {
      color: #a54242;
    }

    ul:nth-child(5n + 2) li:first-child {
      color: #de935f;
    }

    ul:nth-child(5n + 3) li:first-child {
      color: #f0c674;
    }

    ul:nth-child(5n + 4) li:first-child {
      color: #8c9440;
    }

    ul:nth-child(5n + 5) li:first-child {
      color: #85678f;
    }

    li:first-child {
      font-weight: bold;
    }

    li:last-child {
      margin: 0 0 1em;
    }
```

```html
  </style>
</head>
...
```

And to bring everything together, let's make a small cursor blink at the bottom!
First add an ID to the cursor to make it easily identifiable (ba-dum-tss):

```html
...
    </ul>
  </nav>
```

```html-new
  <p><span>user@home &gt; </span> <span id="cursor">_</span></p>
```

```html
</body>
...
```

And now the blink animation:

```html
...
    li:last-child {
      margin: 0 0 1em;
    }

```

```html-new
    p #cursor {
      background-color: #b5bd68;
      animation: blink 2s infinite;
    }

    @keyframes blink {
      0% { opacity: 0; }
      49% { opacity: 0; }
      50% { opacity: 1; }
    }
```

```html
  </style>
</header>
```

![A terminal-like startpage](/images/terminal-like-startpage-3.png)
_- All finished!_

If you've come this far, read through everything, and created your very own
startpage: **well done!**. It can be _very_ confusing, especially for people
who have little to no experience in web development.

Going from here, there are plenty of things you can do to this startpage. Add
more links, change the colors a bit, maybe add a search bar?

If you are looking for the full HTML file,
<a href="/files/terminal-like-startpage/index.html" download>it's here</a>.
You can also check the
[full git repository](https://gitlab.com/vallode/startpage-example/-/tree/terminal).

Be sure to share your creation with us!
