+++
title = "Startpage search bars"
image = "assets/images/search-bar-splash.png"
description = "Check out how to create a simple, pleasant, and feature-packed search bar with a few lines of HTML and the help of DuckDuckGo."
+++
# Startpage search bars

{% banner() %}
This guide uses the startpage from the [basic startpage guide](/guides/basic-startpage)
{% end %}

In this guide we will walk through the creation of a simple search bar to use in
our startpage. Very simple and straight forward, with no JS required.

In our first guide we created a page that had the bare minimal requirements of
a startpage. We will try to spruce it (or another startpage you made) up with
a search bar!

By the time we are done, our startpage will look a little like this:

![Startpage with basic search bar](/images/search-bar-startpage.png)

## Simple stuff

In a few lines of HTML we will have a fully functional search bar on our
startpage!

```html
<form action="https://duckduckgo.com/" method="GET">
  <input autofocus
         name="q"
         type="search"
         placeholder="search with duckduckgo...">
</form>
```

We can use this to replace our "Cool, eh?" header on our page, you'd do this by
replacing the `<h1>` element with the above code. You should then change the
styling of the bar by replacing the `h1 {...}` styles with `form {...}`.

Our search bar will look a little something like this:

![A plain search bar in Firefox](/images/search-bar.png#small)

So this is seriously all you need for a basic search bar. Obviously it will look
a little out of place for this startpage, but we will style it up right now.

## New tab worthy

Search bars are quite a ferocious battleground for opinion, so I will offer four
quite simple styles of search bar you can use in your startpage here.

![Search bar with colourful chevron](/images/search-bar-1.png#small)

![Search bar with dark background](/images/search-bar-2.png#small)

![Semi-transparent search bar](/images/search-bar-3.png#small)

![Search bar with window title](/images/search-bar-4.png#small)

When it comes to user input, the sky really is the limit. Keep it simple or
play around with fancier effects.

You can find some cool search bars on
[CodePen](https://codepen.io/search/pens?q=search). I highly suggest looking
there for inspiration when you are feeling a bit lost!

In lieu of posting all the code here, all of these search bars can be found in
the CodePen collection we have made
[here](https://codepen.io/collection/DRrkQK).

## The mighty bang

In this tutorial we used DuckDuckGo as our search engine, and there is a very
good reason for that.

Apart from being a non-google search engine DDG offers a feature called
[bangs](https://duckduckgo.com/bang).
It refers to what is more commonly known as an exclamation mark!

This feature allows you to type a query like `!nf airplane` and have DDG
redirect you to Netflix's search page. They also have _thousands of others_ you
can choose from.

This means you hardly ever need to reinvent the wheel and write up your own JS
search bar. Since the features most want are right there, in your three lines of
HTML.

Here are some cool bangs I tend to use:

* `!manpage` -> takes you to the manpage of a tool
* `!w` -> takes you to the wikipedia page or search
* `!stackoverflow` -> searches stackoverflow

Obviously people would still want to roll their own solutions, since you may
not like how long some bang "shortcuts" are, though we will have to leave that
for another guide!
