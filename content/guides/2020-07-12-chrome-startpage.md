+++
title = "Setting your new tab page on Chrome"
description = "Learn how to set your new tab to open your local startpage on Chrome, using a single file to make your startpage into an extension."
+++

# Setting your new tab page on Chrome

{% banner() %}
Not using Chrome? Check out our guide for [Firefox](/guides/firefox-startpage)
{% end %}

In general, setting your new tab page to a custom HTML file has become a bit
bothersome. There _are_ extensions out there for both Firefox and Chrome that
fulfil some requirements and I do still recommend that people who are
not comfortable editing files directly stick to those.

[New tab override](https://addons.mozilla.org/en-GB/firefox/addon/new-tab-override/)
is the best one you can find but the core limitation is that it can
only access _a single file_ so you cannot use startpages that rely on other
local files (scripts, stylesheets).

## The manifest.json way

The simplest way to go about setting your new tab page is creating a
`manifest.json` file inside the folder your startpage is in.

Locate your startpage folder (where the `index.html` file is) and create a new
file called `manifest.json`.

Inside this file, we will save the following lines:

```json
{
  "manifest_version": 2,
  "name": "New tab Startpage",
  "version": "1.0.0",
  "description": "Custom new tab startpage.",
  "chrome_url_overrides": {
    "newtab": "index.html"
  }
}
```

The only thing to change here is the `"newtab": "index.html"` line, if your
HTML file is called something else, change this to match it.

With the file saved, navigate to `chrome://extensions/` in your Chrome browser.

Toggle `Developer mode` in the top right.

![Developer mode toggle - On](/images/chrome-startpage.png#small)

This should make three more buttons appear on the left side of your screen.

![Extra buttons](/images/chrome-startpage-2.png#small)

Click `Load unpacked` and select the folder in which your startpage (and the
new `manifest.json` file) is.

Your folder will appear as an extension, you can now toggle developer mode off
and open a new tab.

### Autofocus

Stealing focus from Chrome's address bar is _hard_ and the documentation
essentially states that it should be impossible.

There are methods out there but I currently cannot find one that is reliable
enough to share.

Don't have a startpage? Check out [our guides](/guides) for some simple
startpages you can make in a jiffy!
